import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Quote } from './quote.model';
import { QuoteService } from './quote.service';

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.scss']
})
export class QuoteComponent implements OnInit {

  public quote: Quote;
  public errorMessage: string;

  constructor(private quoteService: QuoteService) { }

  ngOnInit() {
    this.loadQuote();
  }

  /**
   * load a quote to display it
   */
  public loadQuote() {
    this.quoteService.getRandomQuote()
        .subscribe((quote: Quote) => {
          delete this.errorMessage;
          this.quote = quote;
        }, (error: HttpErrorResponse) => {
          console.error(error);
          delete this.quote;
          this.errorMessage = 'Failed to load the data, please try again later.';
        });
  }

}
