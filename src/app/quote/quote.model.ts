export class Quote {

  /**
   * unique id of the quote
   */
  id: string;

  /**
   * author of the quote
   */
  author: string;

  /**
   * text of the quote
   */
  text: string;
}
