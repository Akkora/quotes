import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QuoteService {

  constructor(private http: HttpClient) { }

  /**
   * get random quote of quote-garden api
   */
  public getRandomQuote() {
    return this.http
               .get('https://quote-garden.herokuapp.com/quotes/random')
               .pipe(map((response: any) => {
                 return {
                   id: response._id,
                   author: response.quoteAuthor || 'Unknown Author',
                   text: response.quoteText
                 };
               }));
  }
}
