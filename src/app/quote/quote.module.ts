import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { QuoteComponent } from './quote.component';
import { QuoteService } from './quote.service';

@NgModule({
  declarations: [
    QuoteComponent
  ],
  imports: [
    CommonModule
  ],
  providers: [
    QuoteService
  ],
  exports: [
    QuoteComponent
  ]
})
export class QuoteModule {}
